import numpy as np
from PIL import Image, ImageDraw

WIDTH = 640
HEIGHT = 640
RADIUS = 75
NUM_LINES = 50



canvas = Image.new("RGBA", (WIDTH, HEIGHT), "white")
draw = ImageDraw.Draw(canvas)
draw.ellipse([WIDTH/2 - RADIUS, HEIGHT/2 - RADIUS, WIDTH/2 + RADIUS, HEIGHT/2 + RADIUS], outline="steelblue", width=3)
canvas.save("shape1.png")
